Example: Add a test
===================

**TODO**

To add a test, examine first the configuration toml file's capabilities. Give a simple example of one asset directory with one entrypoint script and the login, etc details.

Here, you should start in reverse: start from a simple experiment with assets, explain its structure, and then how one would write a :ref:`configuration file <conffile>` around it.
