.. testbuddy-g5k documentation master file, created by
   sphinx-quickstart on Mon Jul  8 16:02:06 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to testbuddy-g5k's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   add_test
   configuration_file

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
