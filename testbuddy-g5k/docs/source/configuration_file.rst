.. _conffile:

The configuration file
======================

We provide an example configuration file under :file:`config/default.toml`. In order to "run" it, we would use:

.. code-block:: bash

   testbuddy-g5k --configuration config/default.toml launch

Generally, it is envisioned that you would write your own configuration files to declare the tests you would like to run on Grid'5000. The configuration files are using the TOML format. A configuration file has two sections, :code:`[sync]` and :code:`[launch]`. There are also three keys, :code:`login, project, name` that do not belong in those sections, and you would declare them at the top of the file:

.. code-block:: toml

   login = "g5k-username"
   project = "my-fenics-tests"
   name = "poisson"

This will log in to Grid'5000 with the username set to :code:`g5k-username`. The :code:`project` and :code:`name` keys will have the effect of testbuddy-g5k creating a directory :file:`testbuddy-g5k/my-fenics-tests/poisson`, and putting inside it all declared assets. The experiment is expected to store its results under and :file:`testbuddy-g5k/my-fenics-tests/poisson/results`

Thus in general, see :file:`{site}/testbuddy-g5k/{project}/{name}` is where assets and results are placed, where :code:`site` is the Grid'5000 site on which a particular experiment takes place.

In a single configuration file, only one login, project, and name can be set. However, each experiment can declare its own assets, entrypoint, parameters, site, and grd options (i.e. how many hosts to run on, what type of CPU/GPU/hardware resources to request from Grid'5000, and so on.)

The [sync] section
------------------

These are the configuration options particular to the :code:`sync` subcommand of :code:`testbuddy-g5k`. Currently, there is only

.. code-block:: toml

   results = "/path/to/results"

which will store all obtained tarballs from :code:`testbuddy sync` locally into :code:`/path/to/results`.

The [launch] section
--------------------

In this section we declare the :code:`experiments` array which contains all the experiments that will be executed. Each experiment is a different job on its own resources in Grid'5000. Some options can be the same for all experiments, and are defined in their own keys under :code:`[launch]`, while others are defined within the elements of :code:`experiments`. For example:

.. code-block:: toml

   [launch]
   assets = [
     "/path/to/experiment_sources/",
     "/some/additional/directory/",
     "/some/additional/file/db.sqlite3"
   ]
   entrypoint = "entrypoint.py"
   grd_environment = "debian11-nfs"
   experiments = [
     { site = "rennes", cluster = "paravance", grd_options = "hosts=8" },
     { site = "grenoble", cluster = "dahu", grd_options = "hosts=4" }
   ]

What we call assets here are the source code files that will run an experiment.

This is a configuration of two experiments. In this configuration, both experiments have common assets and a common entrypoint, :file:`entrypoint.py`, as well as common :code:`grd_environment`, but they have different key values for :code:`site, cluster, grd_options`. The assets are placed under :file:`{site}/testbuddy-g5k/{project}/{name}`, and then :code:`entrypoint` is a filepath relative to that directory, i.e. :file:`{site}/testbuddy-g5k/{project}/{name}/{entrypoint}`. The entrypoint script will be executed on one host of those acquired, and typically coordinates all hosts in a cluster to work together for the experiment.

Note that the entrypoint script is passed in its first argument the directory in which it resides (this is because Grid'5000 launches the entrypoint from a different directory) so that the entrypoint can find all experiment assets.

.. admonition:: Keep it simple

   The configuration files allow for some cleverness, but it is best to avoid it and instead maintain simple configuration files, perhaps at the cost of some redundancy.
