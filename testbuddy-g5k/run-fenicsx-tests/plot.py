#!/usr/bin/env python3

from jinja2 import Template
import pandas as pd
import json
from pathlib import Path
import plotly.express as px

def main():

    # Load JSON data into DataFrame.
    many = "/home/fox/code/git/gsoc2024-debian-parsons-ballarin/testbuddy-g5k/many-reports/many.json"
    df = pd.DataFrame()
    with open(many, "r") as f:
        for line in f:
            tmp = pd.json_normalize(json.loads(line))
            df = pd.concat([df, tmp])
    df = df.reset_index(drop=True)
    df["experiment.dof"] = pd.to_numeric(df["experiment.dof"])
    df["experiment.ZZZ_solve.wall_tot"] = pd.to_numeric(df["experiment.ZZZ_solve.wall_tot"])

    # Make the plots and write to HTML file.
    plotly_js = Path(
        "/home/fox/code/git/gsoc2024-debian-parsons-ballarin/testbuddy-g5k/www/plotly-2.32.0.min.js"
    )
    output_html = Path(
        "/home/fox/code/git/gsoc2024-debian-parsons-ballarin/testbuddy-g5k/www/static/strong_poisson.html"
    )
    input_template = Path(
        "/home/fox/code/git/gsoc2024-debian-parsons-ballarin/testbuddy-g5k/www/templates/graph.html.jinja"
    )
    # The Plotly plot.
    dof = df["experiment.dof"][0]
    total_cores = df["grid5000.total_cores"][0]
    hosts = df["grid5000.hosts"][0]
    
    fig = px.line(
        df,
        x="date",
        y="experiment.ZZZ_solve.wall_tot",
        labels={"experiment.ZZZ_solve.wall_tot": "wall time (s)"},
    )
    fig.add_hline(y=3.2, line_dash="dot", annotation_text="average", annotation_position="bottom right")
    fig.add_hrect(y0=2.8, y1=3.6, annotation_text="green zone: no regression", opacity=0.25, fillcolor="green", line_width=0, annotation_position="outside top")
    fig.update_layout(yaxis_range=[0,7])
    fig.update_layout(
        title={
            "text": f"Total wall time in seconds for strong Poisson, {dof} degrees of freedom on {hosts} hosts with {total_cores} cores in total.",
            "x": 0.5,
        }
    )

    # The Jinja2 template.
    template = {
        "fig": fig.to_html(full_html=False, include_plotlyjs=False),
        "g5k_attribute": 'Experiments presented in this page were carried out using the Grid\'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations (see <a href="https://www.grid5000.fr">https://www.grid5000.fr</a>).',
        "title": f"Grid'5000 FEniCS experiments, strong Poisson",
        "plotly_js": plotly_js
    }
    # Generate and write out to HTML file.
    with open(output_html, "w", encoding="utf-8") as f:
        with open(input_template) as template_file:
            jinja_template = Template(template_file.read())
            f.write(jinja_template.render(template))

if __name__ == "__main__":
    main()
