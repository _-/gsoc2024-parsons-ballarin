#+TITLE: Final report on "Benchmarking Parallel Performance of Numerical MPI Packages"
#+SUBTITLE: Google Summer of Code 2024 for Debian
#+AUTHOR: Nikolaos Chatzikonstantinou
#+DESCRIPTION: Final report on "Benchmarking Parallel Performance of Numerical MPI Packages"
#+HTML_DOCTYPE: xhtml-strict
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="stylesheet.css" />

* Acknowledgements

I would like to thank my mentors, Fransesco Ballarin and Drew Parsons, for their help and guidance, and the Grid'5000 team for allowing me to use their testbed for the experiments. Furthermore I'd like to thank Nilesh Patra of Debian for his mediation as an org admin for GSoC. I'd also like to thank Google for the opportunity that was offered to me through the [[https://summerofcode.withgoogle.com/][Summer of Code]] program.

Experiments presented in this page were carried out using the Grid'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations (see <https://www.grid5000.fr>).

* Description

The goal of this project, at a minimum, was to *collect measurements* of the [[https://github.com/FEniCS/performance-test][FEniCS miniapp]], of the [[https://fenicsproject.org/][FEniCS project]], as packaged by [[https://www.debian.org/][Debian]]. The purpose was to *aid in detecting regressions* in the involved Debian packages via the visual inspection of *plotted data points*. Notably, as I understood eventually, this isn't testing upstream but rather tests regressions for how Debian has put together its distribution with regards to FEniCS. Additional goals included creating a general framework in which more tests can be written for other packages such as [[https://www.nwchem-sw.org/][NWChem]] or [[https://meep.readthedocs.io/en/master/][MEEP]].

These measurements were to run on [[https://www.grid5000.fr][Grid'5000]], a large-scale and flexible testbed. Various parameters were to be taken with different values, such as altering the underlying [[https://en.wikipedia.org/wiki/Basic_Linear_Algebra_Subprograms][BLAS]] and [[https://en.wikipedia.org/wiki/Message_Passing_Interface][MPI]] implementations or using a different number of cores or degrees of freedom [fn:dof].

[fn:dof] A parameter to the miniapp, and in particular a parameter of the [[https://en.wikipedia.org/wiki/Finite_element_method][Finite Element Method]], the method used by the FEniCS; the miniapp uses it to solve the partial differential equations of [[https://en.wikipedia.org/wiki/Poisson%27s_equation][Poisson]] and the so-called [[https://en.wikipedia.org/wiki/Linear_elasticity][elastostatics problem]] in 3D.

* Accomplishments

** Testbuddy-g5k

We created the [[https://salsa.debian.org/_-/testbuddy-g5k][testbuddy-g5k]] tool as a general tool that allows the declarative configuration of experiments to be launched to Grid'5000 from another, unrelated, server: to describe it simply, it functions as a script-uploader and results-downloader that requests resources from Grid'5000 and launches scripts in the obtained resources.

Equipped with this tool, we were able to write scripts that would launch experiments for the FEniCS miniapp and store them in an [[https://www.sqlite.org/][SQLite3]] database; we also wrote another script that would produce HTML plots with [[https://plotly.com/graphing-libraries/][Plotly]] [fn:dash].

With this approach, at the cost of some repetition (e.g. having to write a script or two per project), we gained significant flexibility in the way results are stored, and in particular we did not have to think of a future-proof common format for all the potential Debian packages that may need to be tested.

#+CAPTION: A typical use of the testbuddy-g5k tool
#+NAME: fig:testbuddy
#+ATTR_HTML: :width 100%
[[./testbuddy.svg]]

After this sequence of events has taken place, the user will find various plots similar to this one:

#+CAPTION: Artificial weekly data for 4 years, the light region focusing on a regression
#+NAME: fig:speedup
#+ATTR_HTML: :width 100%
[[./speedup.png]]

[fn:dash] Expressly, we avoided [[https://dash.plotly.com/][Dash]] in order to only use Debian packages and to avoid having to configure the Dash webserver.

** Patches to other projects

I added documentation of the timings and improved some C++ code in the FEniCS miniapp:

- [[https://github.com/FEniCS/performance-test/pull/154][FEniCS/performance-test#154]]
- [[https://github.com/FEniCS/performance-test/pull/155][FEniCS/performance-test#155]]

I made various documentation impovements to ReFrame:

- [[https://github.com/reframe-hpc/reframe/pull/3195][reframe-hpc/reframe#3195]]
- [[https://github.com/reframe-hpc/reframe/pull/3196][reframe-hpc/reframe#3916]]
- [[https://github.com/reframe-hpc/reframe/pull/3197][reframe-hpc/reframe#3197]]
- [[https://github.com/reframe-hpc/reframe/pull/3208][reframe-hpc/reframe#3208]]

The following are more minor in nature:

- GNU sleep <https://debbugs.gnu.org/cgi/bugreport.cgi?bug=70946>
- Grid'5000 wiki edits <https://www.grid5000.fr/w/Special:Contributions/Nchatzik>
- kameleon <https://github.com/oar-team/kameleon/pull/130>

* Difficulties

** Segfaults and unsupported Debian 13

I met a large hurdle when I realized that there were segfaults with the miniapp on Debian 12 and general issues with Debian 13 on Grid'5000. In practice, this meant that I would have to wait until the Grid'5000 team would fix its Debian 13 issue, with projected date at least 4 months after the GSoC program would end. I continued developing my program with the following compromise: instead of running the experiments for hundreds or thousands of cores, I would have to restrict myself to a maximum of 3 cores!

** Decision to use a proper database

There didn't seem to exist a convenient way to serialize [[https://pandas.pydata.org/][Pandas]] DataFrame objects [fn:pandas]. For that reason and for the fact that we could liberate ourselves from Python tooling, we opted to use a proper database like SQLite3. This created some additional attrition in implementation process, especially since I am not well-versed with SQL concepts.

** Testing

Due perhaps to my naivete in architectural design, my unfamiliarity with Python testing, and the shortage of time, I was not able to write unit tests for this project. It seems that proper isolation of certain components of the programs I wrote for the purpose of testing would increase the complexity of the software and would also slow me down. In a crunch, I debugged as I used the software; Drew Parsons helped me during the review phase a great deal too.

[fn:pandas] "Tables" in common parlance, from a popular Python library.

* Work ahead

** Writing tests for other packages

There still have not been any tests written for NWChem or any other Debian package. I hope to be able to write some in the future.

** Improving the SQL schema

The SQL schema provided was rushed and does not use indexing optimizations [fn:indexing]. This may be fine since now there is only one package, FEniCS, and one purpose, a complete timeline plot, and thus all records must be traversed. When more packages and more constrainted plots are required (say, on dates), it may be fruitful to include SQL indexing in the schema.

[fn:indexing] SQLite documentation on [[https://sqlite.org/queryplanner.html][how to pick good indices]] may be useful.

* Download

Testbuddy-g5k has a [[https://salsa.debian.org/_-/testbuddy-g5k][git repository]], a [[https://_-.pages.debian.net/testbuddy-g5k][documentation]] page, and a [[https://pypi.org/project/testbuddy-g5k/][PyPI package]].

* Some notes on this document                                      :noexport:

I can get rid of the default CSS with ~#+OPTIONS: html-style:nil~. The CSS options are described at [[info:org#CSS support][org(CSS support)]].

* Org-publish configuration                                        :noexport:

This page can be published with ~M-x org-publish-current-project~, see [[info:org#Publishing][org(Publishing)]]. It may also be published by using ~make(1)~.

This particular keybinding can make it faster:

#+begin_src elisp
  (local-set-key (kbd "C-c m")
   (lambda () (interactive)
     (compile "make -k")))
#+end_src

The following local variables define the Org-publish project:

#+begin_src
Local Variables:
org-publish-project-alist:
  (("gsoc2024"
    :components ("gsoc2024-notes" "gsoc2024-static"))
   ("gsoc2024-notes"
    :base-directory "."
    :base-extension "org"
    :publishing-function org-html-publish-to-html
    :publishing-directory "../public")
   ("gsoc2024-static"
    :base-directory "."
    :base-extension "css\\|png"
    :publishing-function org-publish-attachment
    :publishing-directory "../public"))
org-html-validation-link: ""
End:
#+end_src
